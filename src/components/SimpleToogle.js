export  default  class SimpleToogle
{
    constructor(className)
    {
        this.obj = document.getElementsByClassName(className);
        for (let i = 0; i < this.obj.length; i++){
            this.obj[i].lastElementChild.style.maxHeight = "0px";
            this.obj[i].lastElementChild.style.opacity = "0";
            this.obj[i].lastElementChild.style.overflow = "hidden";
        }
    }

    init()
    {
        let _this = this;
        for (let i = 0; i < this.obj.length; i++){
            this.obj[i].firstElementChild.onclick = function (e) {
                e.preventDefault();
                _this.changeDisplay(_this.obj[i].lastElementChild);
            }
        }
    }

    changeDisplay(obj)
    {
        if(obj.style.maxHeight == "0px"){
            obj.style.maxHeight = "100%";
            obj.style.opacity = "1";
        }else{
            obj.style.maxHeight = "0px";
            obj.style.opacity = "0";
        }
    }
}