import SimpleToogle from './SimpleToogle';

export default class FadeToogle extends SimpleToogle
{
    constructor()
    {
        super(...arguments);
        for (let i = 0; i < this.obj.length; i++){
            this.obj[i].lastElementChild.style.transition = "opacity .3s, max-height .3s";
        }
    }
}