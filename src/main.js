import SimpleToogle from './components/SimpleToogle';
import FadeToogle from './components/FadeToogle';


let obSimpleCollapse = new SimpleToogle('simple-collapse');
obSimpleCollapse.init();

let obFadeCollapse = new FadeToogle('fade-collapse');
obFadeCollapse.init();